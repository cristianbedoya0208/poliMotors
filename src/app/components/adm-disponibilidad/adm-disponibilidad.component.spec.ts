import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmDisponibilidadComponent } from './adm-disponibilidad.component';

describe('AdmDisponibilidadComponent', () => {
  let component: AdmDisponibilidadComponent;
  let fixture: ComponentFixture<AdmDisponibilidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmDisponibilidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmDisponibilidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
